﻿using System;
using System.Net;
using System.Web.Http;

namespace WebApplication1.Controllers
{
    [AllowAnonymous]
    public class HomeController : ApiController
    {
        [HttpGet]
        public RES Index()
        {
            return new RES { Status = HttpStatusCode.OK, Msg = "Success" };
        }

        // api/home/login
        [HttpPost]
        public RES Login(LOGIN user)
        {
            USER chkdb = new Library().GetLoginByUserName(user.UserName, out string msg);
            if (msg == null)
            {
                return new RES { Status = HttpStatusCode.OK, Data = chkdb };
            }
            else
            {
                return new RES { Status = HttpStatusCode.Unauthorized, Msg = msg };
            }
        }
    }

    public class USER
    {
        public long UserID { get; set; }
        public string UserName { get; set; }
        public string UserGroup { get; set; }
        public string LongName { get; set; }
        public string Asset { get; set; }
        public string Token { get; set; }
        public string created_by { get; set; }
        public DateTime created_date { get; set; }
        public string updated_by { get; set; }
        public DateTime? updated_date { get; set; }
    }

    public class LOGIN
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
    }
}
