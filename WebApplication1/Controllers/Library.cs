﻿using System.Collections.Generic;

namespace WebApplication1.Controllers
{
    public class Library
    {
        public USER GetLoginByUserName(string userName, out string msg)
        {
            msg = null;

            /* get from DB */
            //msg = "Wrong Username and or Password";
            //msg = "Username not registered";
            USER login = new USER { UserName = userName, UserGroup = "Admin", LongName = "Long Name", Asset = "2" };

            login.Token = JwtManager.GenerateToken(userName, login.UserGroup, login.Asset);
            return login;
        }

        public List<USER> GetUsers()
        {
            /* get from DB */
            return new List<USER> { new USER { UserName = "ade", UserGroup = "Admin" }, new USER { UserName = "imron", UserGroup = "Admin" } };
        }

        public USER GetUser(string id)
        {
            /* get from DB */
            return new USER { UserName = id, UserGroup = "Admin" };
        }

        public CUD PostLogin(USER bu)
        {
            /* save to DB */
            return new CUD { Id = bu.UserID, AffectedRows = 1 };
        }

        public CUD DeleteLogin(long id)
        {
            /* delete from DB */
            return new CUD { Id = id, AffectedRows = 1 };
        }
    }
}