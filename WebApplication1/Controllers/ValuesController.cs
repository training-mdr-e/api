﻿using System;
using System.Net;
using System.Web.Http;

namespace WebApplication1.Controllers
{
    public class ValuesController : BaseController
    {
        [HttpGet]
        public RES Users()
        {
            RES res = new RES { Status = HttpStatusCode.OK };
            res.Data = new Library().GetUsers();
            return Res(res);
        }

        [HttpGet]
        public RES Users(string id)
        {
            RES res = new RES { Status = HttpStatusCode.OK };
            res.Data = new Library().GetUser(id);
            return Res(res);
        }

        [HttpPost]
        public RES Users(USER bu)
        {
            RES res = new RES { Status = HttpStatusCode.OK };
            try
            {
                if (bu.UserID == 0)
                {
                    bu.created_by = GetUserName();
                    bu.created_date = DateTime.Now;
                }
                else
                {
                    bu.updated_by = GetUserName();
                    bu.updated_date = DateTime.Now;
                }
                res.Data = new Library().PostLogin(bu);
            }
            catch (Exception e)
            {
                res.Status = HttpStatusCode.InternalServerError;
                try
                {
                    res.Msg = e.InnerException.InnerException.Message;
                }
                catch (Exception)
                {
                    res.Msg = e.Message;
                }
            }
            return Res(res);
        }

        [HttpDelete]
        public RES Users(long id)
        {
            RES res = new RES { Status = HttpStatusCode.OK };
            try
            {
                res.Data = new Library().DeleteLogin(id);
            }
            catch (Exception e)
            {
                res.Status = HttpStatusCode.InternalServerError;
                try
                {
                    res.Msg = e.InnerException.InnerException.Message;
                }
                catch (Exception)
                {
                    res.Msg = e.Message;
                }
            }
            return Res(res);
        }
    }
}
