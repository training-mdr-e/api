﻿using WebApplication1.Filters;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace WebApplication1.Controllers
{
    [JwtAuthentication]
    public class BaseController : ApiController
    {
        public string GetUserName()
        {
            string stream = ControllerContext.Request.Headers.Authorization.Parameter;
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            JwtSecurityToken tokenS = handler.ReadToken(stream) as JwtSecurityToken;
            return tokenS.Claims.First(claim => claim.Type == "unique_name").Value;
        }

        public string GetUserGroup()
        {
            string stream = ControllerContext.Request.Headers.Authorization.Parameter;
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            JwtSecurityToken tokenS = handler.ReadToken(stream) as JwtSecurityToken;
            return tokenS.Claims.First(claim => claim.Type == "role").Value;
        }

        public string GetAsset()
        {
            string stream = ControllerContext.Request.Headers.Authorization.Parameter;
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            JwtSecurityToken tokenS = handler.ReadToken(stream) as JwtSecurityToken;
            return tokenS.Claims.First(claim => claim.Type == "asset").Value;
        }

        public RES Res(RES res)
        {
            if (res.Total == 0)
            {
                try
                {
                    res.Total = res.Data.Count;
                }
                catch (Exception)
                {

                }
            }
            res.Token = JwtManager.GenerateToken(GetUserName(), GetUserGroup(), GetAsset());
            return res;
        }
    }

    public class RES
    {
        public HttpStatusCode Status { get; set; }
        public dynamic Data { get; set; }
        public string Msg { get; set; }
        public string Token { get; set; }
        public int Total { get; set; }
    }

    public class CUD
    {
        public long Id { get; set; }
        public int AffectedRows { get; set; }
    }
}
